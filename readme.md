# PRESENTATION

## care 1

Narayan :

l'exercice demandait de faire un DASH pour visulaiser tout en une page.
Pour installer DASH il faut faire :

```sh
pip install dash
```

pour connecter dash à l'api fastAPI on a utilisé requests
pour installer requests library il faut faire :

```sh
pip install requests
```

puis on a import pandas library pour traiter les donner 
pour installer panda library il faut faire : 

```sh
pip install pandas
```

on a utiliser geopandas pour traiter les données géospatiales en python.
pour installer geopandas il faut faire :

```sh
pip install geopandas
```

on a utilisé plotly pour générer des cartes en python et visualiser les données grace à geopandas.
pour installer plotly il faut faire :

```sh
pip install plotly
```

## carte 2 

on a utilisé les mêmes dépendances que pour la carte 1

## carte 3 

on a utilisé les mêmes dépendances que pour les cartes 1 et 2 et on a utilisé Folium pour générer les lignes sur la carte.
pour installer folium il faut faire :

```sh
pip install folium
```

## dropdown 

Angelo
pour générer le dropdown on a utilisé les mêmes dépendances que pour les cartes 1 et 2

## vue

pour générer le dropdown on a utilisé les mêmes dépendances que pour les cartes 1 et 2 et dropdown et on a utilisé plotly.graph_objects pour visulaiser les statitiques.
et il faut installer la dépendance collections :

```sh
pip install collection
```
