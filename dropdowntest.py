import dash
from dash import dcc, html, dash_table
from dash.dependencies import Input, Output
import pandas as pd
import json

file_path = 'testdonnee.json'

with open(file_path, 'r') as f:
    data = json.load(f)

df = pd.json_normalize(data, record_path='voyages', meta=['nom', 'prenom', 'pays_residence'], errors='ignore')

countries = df['pays_residence'].unique()

app = dash.Dash(__name__)

app.layout = html.Div([
    dcc.Dropdown(
        id='country-dropdown',
        options=[{'label': i, 'value': i} for i in countries],
        value=countries[0] if len(countries) > 0 else None  
    ),
    dash_table.DataTable(id='table')  
])

@app.callback(
    Output('table', 'data'),
    [Input('country-dropdown', 'value')]
)
def update_table(country):
    filtered_df = df[df['pays_residence'] == country]
    return filtered_df.to_dict('records')

if __name__ == '__main__':
    app.run_server(debug=True)
