import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output
import plotly.graph_objects as go
import pandas as pd
import json
from collections import defaultdict

file_path = 'testdonnee.json'
with open(file_path, 'r') as file:
    data = json.load(file)

visits_by_country = defaultdict(int)
total_days_by_country = defaultdict(int)
total_days_by_person = defaultdict(lambda: 0)

for person in data:
    person_name = f"{person['prenom']} {person['nom']}"
    for voyage in person['voyages']:
        pays = voyage['pays']
        nb_jours = voyage['nb_jour_visite']
        visits_by_country[pays] += 1
        total_days_by_country[pays] += nb_jours
        total_days_by_person[person_name] += nb_jours

most_visited_country = max(visits_by_country, key=visits_by_country.get)
average_visit_duration = {pays: total_days_by_country[pays] / visits_by_country[pays] for pays in total_days_by_country}
most_traveled_person = max(total_days_by_person, key=total_days_by_person.get)

app = dash.Dash(__name__)

app.layout = html.Div([
    html.H1("Statistiques de Voyages"),
    dcc.Dropdown(
        id='dropdown',
        options=[
            {'label': 'Pays le plus visité', 'value': 'most_visited_country'},
            {'label': 'Moyenne de la durée des visites par pays', 'value': 'average_visit_duration'},
            {'label': 'Personne ayant le plus voyagé', 'value': 'most_traveled_person'}
        ],
        value='most_visited_country'
    ),
    dcc.Graph(id='graph')
])

@app.callback(
    Output('graph', 'figure'),
    [Input('dropdown', 'value')]
)
def update_graph(selected_dropdown_value):
    if selected_dropdown_value == 'most_visited_country':
        fig = go.Figure(go.Indicator(
            mode = "number",
            value = visits_by_country[most_visited_country],
            title = {"text": f"Pays le plus visité: {most_visited_country}"}
        ))
    elif selected_dropdown_value == 'average_visit_duration':
        fig = go.Figure(data=[go.Bar(
            x=list(average_visit_duration.keys()),
            y=list(average_visit_duration.values()),
            text=list(average_visit_duration.values()),
            textposition='auto'
        )])
        fig.update_layout(title_text='Moyenne de la durée des visites par pays', xaxis_title='Pays', yaxis_title='Moyenne des jours')
    else:
        fig = go.Figure(go.Indicator(
            mode = "number",
            value = total_days_by_person[most_traveled_person],
            title = {"text": f"Personne ayant le plus voyagé: {most_traveled_person}"}
        ))
    return fig

if __name__ == '__main__':
    app.run_server(debug=True)
