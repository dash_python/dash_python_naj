# Les infos demandées 

## ce que nous avons besoin

### Carte 1 :

faire la carte de l'Europe (la liste des pays, a demander au g1)
total de nombre de jours par pays d'Europe. (aller le demander au g1 d'ajouter la durée "en jours" ou date année mois jour obligatoire. G3 on veut la route qui indique le nombre total de jours totals passé par pays).
faire le DASH pour les cartes. 

route : @app.get("/country/days_visit")

g3 : les données arriveront comme ça :

```json
[
{
    "pays":"string",
    "nb_jour_visite": int
}
]
```

pays = pays de départ
nb_jour_visite = indique le nombre de jours passés dans le pays

- Jean : Dash statique, carte de l'Europe
- Narayan : Dash dynamique, route fastapi
- Angelo : Merge et communication.


### Carte 2 :

total nombre de personne par pays : (la route qui indique le total de nombre de personnes par  pays visités, route g3. liste des personnes, liste des pays, et destination par personnes g1.)

route : @app.get("/person/travel_to")

g3 : les données arriveront comme ça :

```json
[
{
    "nom": "string",
    "prenom": "string",
    "pays_residence": "string",
    "voyages":{
        [
            "pays":"string",
            "nb_jour_visite": int
        ]
    }
}
]
```

pays_residence = pays de départ
pays = pays d'arrivée

- Jean : Dash statique, carte de l'Europe
- Narayan : Dash dynamique, route fastapi
- Angelo : Merge et communication.

### Carte 3 :

le pays de départ et le pays de destination par personnes g1, une route qui indique une liste de personne avec leur pays de départ et leur pays d'arriver g3.

route : @app.get("/person/travel_to")

g3 : les données arriveront comme ça :

```json
[
{
    "nom": "string",
    "prenom": "string",
    "pays_residence": "string",
    "voyages":{
        [
            "pays":"string",
            "nb_jour_visite": int
        ]
    }
}
]
```

pays_residence = pays de départ
pays = pays d'arrivée

- Jean : Dash statique, carte de l'Europe
- Narayan : Dash dynamique, route fastapi
- Angelo : Merge et communication.


## Tableau doopdown

pays d'origine dans le dropdown, qui ouvre sur un tableau ou il y a les pays de destination et le nombre total de personnes les ayant visité. (la route qui indique le pays d'origine, le pays de destination et le nombre total des visiteurs de ce pays.)

route : @app.get("/person/travel_to")

```json
[
{
    "nom": "string",
    "prenom": "string",
    "pays_residence": "string",
    "voyages":{
        [
            "pays":"string",
            "nb_jour_visite": int
        ]
    }
}
]
```

pays_residence = pays de départ
pays = pays d'arrivée
nb_jour_visite = indique le nombre de jours passés dans le pays

- Jean : 
- Narayan : 
- Angelo : Merge, Dash dynamique avec tableau dropdown

## vue des stats

liste des statistiques recherchés sur fastapi avec les routes correspondantes. (demander au g3 la liste de leurs statistiques à afficher.)

les stats à afficher sont :

pays le plus visité, 
moyenne de la durée des visites par pays, 
personne ayant le plus voyagé

route à déterminer avec le g3

- Jean : 
- Narayan :
- Angelo : Merge, faire la vue
